# Introducción

Por medio del presente trabajo se pretende, por supuesto aparte de resolver las consignas solicitadas, mostrar y demostrar un conjunto de los features más importantes que están presentes en cualquier aplicación de software y algunos que son específicos de las aplicaciones basadas en arquitectura de microservicios.

Los features considerados y desarrollados son:

- Desarrollo de test unitarios
- Desarrollo de test de integración con objetos “mockeados”
- Se trabajó la idempotencia en los test que por propia naturaleza no lo son como es el caso de los servicios realizados mediante el verbo POST. Para ello se desarrolló un CRUD de Productos y fue tratado y resuelto este tema en las llamadas de creación de un nuevo producto (llamada POST).
- Se realizó la securización de mircoservicios, este trabajo se llevó acabo con Spring Security sobre el microservicio *config-service*. Esto podrá observarse en el Postman en unas llamadas registradas donde se hacen pruebas sobre este microservicio
- Se implementó un ejemplo de llamadas y comunicación entre microservicios
- Se desarrolló un mecanismo de prueba a fallos como sería el caso en el que se caiga un microservicio cuando es llamado por otro. Esto fue implementado en el microservicio *priceschedule-service* sobre el mic *brand-service*.Para ello una vez que estén todos los mic levantados se debe detener el mic *brand-service* y se podrá ver que la llamada NO arrojará error sino que devolverá una respuesta a partir del mecanismo de failback.
- Se desarrollo un monitor de métricas de microservicios reportados por estos a un microservicio centralizado dedicado a esta tarea
- Se desarrolló un microservicio encargado de gestionar, descubrir y poner en funcionamiento los microservicios
- Se desarrolló un microservicio que centraliza las configuraciones de todo el sistema de microservicios de modo tal que se evita el tener que tener que recompilar los microservicios ante posibles cambios


# Arquitectura propuesta

Dado que no se tiene información de la arquitectura actual del dominio, se propuso una solución basada en 3 servicios de dominio y 4 servicios de infraestructura a saber:

- Servicio de gestón de productos - product-service
- Servicio de gestión de marcas - brand-service
- Servicio de planificación de precios - **priceschedule-service**
- Servicio de centralización de configuraciones - config-service
- Servicio de monitorización de métricas - admin-service
- Servicio de descubrimiento de microservicios - registry-service
- Servicio entrypoint, gateway de acceso - gateway-service

El microservicio **priceschedule-service** es el más importante ya que es el cual realiza las tareas específicas solicitadas en el trabajo “TestJava2020 - Tech Lead.txt”.

Para dar un poco más de complejidad al trabajo solicitado el FOREING\_KEY de la cadena del grupo se implementó mediante la comunicación entre microservicios solicitando la información de la cadena asociada a un precio determinado.Esto es, el microservicio *priceschedule-service* realiza una llamada a los microservicios *product-service* y *brand-service* para popular dicha información.

![](doc/Aspose.Words.5972bf45-54fd-436e-befb-264da0cda12c.001.png)

# Decisiones tecnológicas utilizadas

A continuación se enumeran las tecnologías, frameworks, plugins y otras piezas de software que conforman el stack tecnológico de la solución:

- JAVA 11
- Gradle 7.3.3
- Spring Boot 2.3.2
- Spring Boot Actuator
- Spring Data Jpa
- Spring Validation - Hibernate validator
- Spring  Web
- Spring Cloud Config (Client)
- Spring Cloud Config (Server)
- Eureka Discovery Client SPRING CLOUD DISCOVERY
- Eureka Discovery Server SPRING CLOUD DISCOVERY
- OpenFeign SPRING CLOUD ROUTING
- Spring Boot Admin (Client)
- Spring Boot Admin (Server)
- Spring Cloud Hystrix y Hystrix dashboard
- Spring Cloud Sleuth
- Spring Lombok
- Spring Security
- Gateway SPRING CLOUD ROUTING
- H2 Database
- Docker

# Prerrequisitos

La aplicación ha sido construida en un entorno LINUX por lo que todos los scrips que se hacen uso tanto para compilar, deploy y restablecer servicios en docker están desarrollados desde el shell del mensionado sistema operativo.

NO obstante como lo que se encuentran esos scripts son comandos dockers pueden ser ejecutados invidualmente desde la cualquier otra consola sin hacer uso de los shell scripts

Para instalar la solución construida se debe tener en cuenta los siguientes puntos:

- Tener instalado docker y docker compose
- Tener disponible por fefault los puertos 7000, 7002, 7003, 7004, 7005, 7006, 7007. Si se desea estos puertos pueden ser modificados desde el archivo *.env*

# Instalación

Antes de enumerar los pasos a realizar es necesario ampliar lo que ya se ha comentado el microservicio *config-service*. Este* tomará las configuraciones correspondientes desde un repositorio GIT y es por ello que es necesario tener acceso en tal repositorio ya que habrá que pasarle el username y password.

1. Hacer un fork de la solución desde la siguiente URL de modo de poder tener control (acceso) de la nueva copia:[ ](https://dguimar@bitbucket.org/techleadtest/teachleadtest.git)<https://dguimar@bitbucket.org/techleadtest/teachleadtest.git>
1. Opcional - No debería ser necesario hacer ninguna modificación en el código ya que el repositorio donde se encuentra la app es público, pero si fuera necesario o se cambia la ubicación del repositorio, desde el proyecto *config-service* dirigirse a la carpeta *src/main/resources/bootstrap.yml*  y modificar la *URI* del repositorio GIT donde quedó alojado el fork del proyecto, colocar las correspondientes credenciales *\$\{GIT\_USERNAME\}* y *\$\{GIT\_PASSWORD\}*
1. Finalmente compilar y levantar los microservicios servicios
    1. COMPILAR: Ejecutar el script *docker\_compile.sh*
    1. DEPLOY: Ejecutar el script *docker-compose up -d*
    1. *REINICIAR MICROSERVICIOS:* Una vez verificado que el microservicio de config-service haya levantado correctamente ejecutar el script *docker\_restart.sh. (Leer la nota IMPORTANTE siguiente)*

**IMPORTANTE:**

El orden de deploy de los microservicios NO es importante a **excepción**, obviamente, del microservicio *config-service* que es quien levanta todas las configuraciones para el resto del sistema. Luego, dado que docker compose, si bien se puede gestionar el orden de deploy por medio de la sentencia "*dependes\_on"* esto solo asegura el orden de instanciación de los contenedores pero no controla ni mucho menos asegura el orden del deploy del microservicio dentro de estos (que es lo que interesa).

Por lo tanto luego que se levantaron todos los contenedores con "*docker compose*", verificar desde la URL: [](http://localhost:7000)<http://localhost:7000> que el servicio *config* responda y una vez esto reiniciar los microservicios restantes ejecutando el script *docker\_restart.sh.*

Adicionallmente a verificar que el microservicio de config haya levantado, verificar que esté cargando los archivos de configuración correctamente para lo cual ejecutar desde el Postman cualquiera de los servicios que se encuentran en la carpeta "Spring config" lo que debería devolver las configuraciones correspondientes

Finalmente esperar que EUREKA [](http://localhost:7003)<http://localhost:7003> descubra a todos los microservicios y listo

# Scripts adicionales construidos

- COMPILAR: *docker\_compile.sh* - Compila la aplicacón por medio de un contenedor Docker.
**IMPORTANTE**: la primera vez que se compile el proceso tardará unos minutos ya que se bajarán todas las depencias necesarias en el proyecto, en compilaciones sucesivas será mucho más rápido por que se está utilizando un volument de cache para agilizar el proceso
- DEPLOY: *docker-compose up -d* - Despliega todos los microservicios con docker compose
- DOCKER CLEAN: *docker\_compile.sh* - Elimina todos los contenedores e imagenes de docker para realizar una instalación "limpia"
- REINICIAR MICROSERVICIOS: *docker-compose up -d* - Reinicia los microservicios para ejecutar luego que el microservicio de config haya sido levantado. Una vez ejecutado, esperar que EUREKA levante y descubra al resto de los microservicios. Ver imagen correspondiente

# Probar la aplicación

ABRIR EN UN NAVEGADOR:

**CONFIG**: [](http://localhost:7000)<http://localhost:7000> y verificar que se muestre la siguiente pantalla:

![](doc/Aspose.Words.5972bf45-54fd-436e-befb-264da0cda12c.002.png)

​**IMPORTANTE:** Este es el microservicio INICIAL y fundamental para que el resto de los microservicios se levanten. Una vez que se haya levantado el config quizás sea necesario reiniciar el resto de los microservicios haciendo uso del script *docker\_restart.sh* como se explica en el apartado de instalacón.

**EUREKA**: [](http://localhost:7003)<http://localhost:7003> y esperar que se carguen los MIC *Brand, Product, Priceschedule y Gateway*

![](doc/Aspose.Words.5972bf45-54fd-436e-befb-264da0cda12c.003.png)

**ADMIN**:[](http://localhost:7002)<http://localhost:7002> y luego presionar en el menu Wallboard desde el cual se podrá evaluar métricas de los microservicios

![](doc/Aspose.Words.5972bf45-54fd-436e-befb-264da0cda12c.004.png)

**FALLBACK**:[ ](http://localhost:7007/hystrix)<http://localhost:8097/hystrix> y luego agregar el siguiente endpoint donde se escucharán los eventos:

[*http://localhost:7007/actuator/hystrix.stream*](http://localhost:8097/actuator/hystrix.stream)

Una vez ingresado en el dashboard de Histrix, para probarlo detener el MIC *Brand* y solicitar nuevamente una consulta al MIC de /prices (desde Postman) y se podrá observar que el MIC seguirá funcionando y se populará el Brand con datos

a partir de un Fallback.

![](doc/Aspose.Words.5972bf45-54fd-436e-befb-264da0cda12c.005.png)


**ENDPOINTS** Para ello se adjunta un Postman collection donde se encontrarán todas las llamadas a los endpoint solicitadas en la consigna de la evaluación, esto es:

- Test 1: petición a las 10:00 del día 14 del producto 35455   para la brand 1 (ZARA)
- Test 2: petición a las 16:00 del día 14 del producto 35455   para la brand 1 (ZARA)
- Test 3: petición a las 21:00 del día 14 del producto 35455   para la brand 1 (ZARA)
- Test 4: petición a las 10:00 del día 15 del producto 35455   para la brand 1 (ZARA)
- Test 5: petición a las 21:00 del día 16 del producto 35455   para la brand 1 (ZARA)

Una respuesta posible tendrá un formato siminlar al siguiente (lo que está pintado en rojo son los datos solicitados en el test):

´´´
{

          "id": 1,                                

          "brandId": 1,                                

          "startDate": "2020-06-14T00:00:00.000+00:00",                                

          "endDate": "2020-12-31T23:59:59.000+00:00",                                

          "rate": {                                

                  "id": 1,                                

                  "name": "Beginning of season1",                                

                  "discountRate": 8.86,                                

                  "description": "Discounted start of season",                                

                  "status": "CREATED",                                

                  "createAt": "2022-01-12T15:53:50.725+00:00"                                

          },                                

          "productId": 35455,                                

          "priority": 0,                                

          "price": 35.5,                                

          "curr": "EUR",                                

          "brand": {                                

                  "id": 1,                                

                  "numberID": "32404580",                                

                  "name": "Zara",                                

                  "description": "Casa de indumentaria",                                

                  "email": "zara@gmail.com",                                

                  "website": "http://www.zara.com",                                

                  "logo": "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fd/Zara\_Logo.svg/245px-Zara\_Logo.svg.png",                                

                  "region": {                                

                                  "id": 4,                                

                                  "name": "Europa"                                

                  },                                

                  "state": "CREATED"                                

          },                                

          "product": {                                

                  "id": 35455,                                

                  "name": "BLACK SANDALS",                                

                  "description": "Zara Basic Collection Black & Silver Flat Sandals Zara - Black - Sandals",                                

                  "stock": 5,                                

                  "price": 38.95,                                

                  "status": "CREATED",                                

                  "category": {                                

                                  "id": 1,                                

                                  "name": "shoes"                                

                  }                                

          }                                

}                                

