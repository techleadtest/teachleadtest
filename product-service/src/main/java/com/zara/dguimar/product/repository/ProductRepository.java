package com.zara.dguimar.product.repository;

import com.zara.dguimar.product.entity.Category;
import com.zara.dguimar.product.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ProductRepository  extends JpaRepository<Product, Long> {

    public List<Product> findByCategory(Category category);
}