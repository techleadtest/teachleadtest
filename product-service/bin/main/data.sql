INSERT INTO tbl_categories (id, name) VALUES (1, 'shoes');
INSERT INTO tbl_categories (id, name) VALUES (2, 'dress');
INSERT INTO tbl_categories (id, name) VALUES (3, 'perfume');

INSERT INTO tbl_products (id, name, description, stock,price,status, create_at,category_id)
VALUES (35455, 'BLACK SANDALS','Zara Basic Collection Black & Silver Flat Sandals Zara - Black - Sandals',5,38.95,'CREATED',NOW(),1);

INSERT INTO tbl_products (id, name, description, stock,price,status, create_at,category_id)
VALUES (35456, 'VESTIDO JACQUARD ESTAMPADO ','Vestido de cuello subido y manga larga',4,12.5,'CREATED',NOW(),2);


INSERT INTO tbl_products (id, name, description, stock,price,status, create_at,category_id)
VALUES (35457, 'YELLOW VELVET SPLASH 150 ML','EAU de toilette floral. la pirámide olfativa incluye notas de vainilla, pera y orquídea.',12,40.06,'CREATED',NOW(),3);
