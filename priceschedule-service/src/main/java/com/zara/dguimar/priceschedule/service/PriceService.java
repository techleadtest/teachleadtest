package com.zara.dguimar.priceschedule.service;

import com.zara.dguimar.priceschedule.entity.Price;

import java.util.Date;

public interface PriceService {
    public Price getPriorityPrice(Date date, Long productId, Long braandId);
}
