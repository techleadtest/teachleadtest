package com.zara.dguimar.priceschedule.entity;

import com.zara.dguimar.priceschedule.model.Brand;
import com.zara.dguimar.priceschedule.model.Product;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Data
@Table(name = "tbl_prices")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Price {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "la cadena del grupo no puede ser vacia")
    @Column(name = "brand_id")
    private Long brandId;

    @NotEmpty(message = "La fecha de inicio no debe ser vacío")
    @Column(name = "start_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;

    @Column(name = "end_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "price_list")
    @JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
    private Rate rate;

    @NotEmpty(message = "El producto no debe ser vacío")
    @Column(name = "product_id")
    private Long productId;

    private Integer priority;
    private Double price;
    private String curr;

    @Transient
    private Brand brand;

    @Transient
    private Product product;

}
