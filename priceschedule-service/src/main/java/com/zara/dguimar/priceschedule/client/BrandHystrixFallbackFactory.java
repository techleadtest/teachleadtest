package com.zara.dguimar.priceschedule.client;

import com.zara.dguimar.priceschedule.model.Brand;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class BrandHystrixFallbackFactory implements BrandClient {
    @Override
    public ResponseEntity<Brand> getBrand(long id) {
        Brand brand = Brand.builder()
                .name("none")
                .numberID("none")
                .description("none")
                .email("none")
                .website("none")
                .logo("none").build();
        return ResponseEntity.ok(brand);
    }
}
