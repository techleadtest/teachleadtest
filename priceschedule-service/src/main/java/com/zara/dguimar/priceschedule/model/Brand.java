package com.zara.dguimar.priceschedule.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Builder
public class Brand {
    private Long id;
    private String numberID;
    private String name;
    private String description;
    private String email;
    private String website;
    private String logo;
    private Region region;
    private String state;
}
