package com.zara.dguimar.priceschedule.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Date;

@Entity
@Data
@Table(name = "tbl_rates")
@AllArgsConstructor @NoArgsConstructor @Builder
public class Rate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "El nombre de la tarifa no puede ser vacío")
    @Column(name="name", nullable=false)
    private String name;

    @Column(name = "discount_rate")
    private Double discountRate;

    private String description;

    private String status;

    @Column(name = "create_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createAt;
}
