package com.zara.dguimar.priceschedule.service;

import com.zara.dguimar.priceschedule.client.BrandClient;
import com.zara.dguimar.priceschedule.client.ProductClient;
import com.zara.dguimar.priceschedule.entity.Price;
import com.zara.dguimar.priceschedule.model.Brand;
import com.zara.dguimar.priceschedule.model.Product;
import com.zara.dguimar.priceschedule.repository.PriceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@RequiredArgsConstructor
public class PriceServiceImpl implements PriceService{

    private final PriceRepository priceRepository;

    @Autowired
    private BrandClient brandClient;

    @Autowired
    private ProductClient productClient;

    @Override
    public Price getPriorityPrice(Date date, Long productId, Long braandId) {
        Price price = priceRepository.findPriorityPriceByRangeDateAndProductIdAndBrandId(date,productId,braandId);
        if(null != price){
            Brand brand = brandClient.getBrand(price.getBrandId()).getBody();
            Product product = productClient.getProduct(price.getProductId()).getBody();
            price.setBrand(brand);
            price.setProduct(product);
        }
        return price;

    }
}
