package com.zara.dguimar.priceschedule.repository;

import com.zara.dguimar.priceschedule.entity.Price;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface PriceRepository extends JpaRepository<Price,Long> {
    public List<Price> findByBrandId(Long brandId );

    @Query("SELECT p FROM Price p WHERE (p.startDate <= :pDate AND p.endDate >= :pDate)")
    public List<Price> findAllPricesByRangeDate(@Param("pDate") Date date);

    @Query("SELECT p FROM Price p WHERE (p.startDate <= :pDate AND p.endDate >= :pDate) AND " +
            "p.productId = :pId " +
            "AND p.brandId = :bId " +
            "AND p.priority = " +
            "(SELECT MAX(p.priority) FROM Price p WHERE (p.startDate <= :pDate AND p.endDate >= :pDate) " +
            "AND p.productId = :pId AND p.brandId = :bId)" )
    public Price findPriorityPriceByRangeDateAndProductIdAndBrandId(@Param("pDate") Date date,
                                                                   @Param("pId") Long productId,
                                                                   @Param("bId") Long brandId);

}
