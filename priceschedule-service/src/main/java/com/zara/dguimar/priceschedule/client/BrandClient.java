package com.zara.dguimar.priceschedule.client;

import com.zara.dguimar.priceschedule.model.Brand;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient(name = "brand-service", fallback = BrandHystrixFallbackFactory.class)
public interface BrandClient {

    @GetMapping(value = "/brands/{id}")
    public ResponseEntity<Brand> getBrand(@PathVariable("id") long id);
}
