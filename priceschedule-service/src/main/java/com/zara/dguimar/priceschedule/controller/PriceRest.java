package com.zara.dguimar.priceschedule.controller;

import com.zara.dguimar.priceschedule.entity.Price;
import com.zara.dguimar.priceschedule.service.PriceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.text.ParseException;
import java.text.SimpleDateFormat;

@Slf4j
@RestController
@RequestMapping("/prices")
public class PriceRest {

    @Autowired
    PriceService priceService;

    // -------------------Recupera el precio correspondiente ------------------------------------------
    @GetMapping
    public ResponseEntity<Price> getPriorityPrice(
            @RequestParam(required = true) String date,
            @RequestParam(required = true) String productId,
            @RequestParam(required = true) String brandId) throws ParseException {
        try {
            Long p = Long.parseLong(productId);
            Long b = Long.parseLong(brandId);
            Price price = priceService.getPriorityPrice(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date), p, b);

            if (null == price) {
                log.warn("No existe precio para la combinación seleccionada {}", productId, brandId);
                return ResponseEntity.noContent().build();
            }
            return ResponseEntity.ok(price);
        } catch (NumberFormatException | ParseException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Los parámetros 'productId' y 'brandId ' deben " +
                    "ser enteros, el parámetro 'date' debe tener el formato 'yyyy-MM-dd HH:mm:ss', ejemplo '2020-06-14 00:00:00' ");
        }

    }
}
