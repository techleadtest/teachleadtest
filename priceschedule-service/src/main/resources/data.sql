INSERT INTO tbl_rates (id, name, description, discount_rate, create_at, status) VALUES(1, 'Beginning of season1', 'Discounted start of season', 8.86, NOW(),'CREATED');
INSERT INTO tbl_rates (id, name, description, discount_rate, create_at, status) VALUES(2, 'Visa promotion1', 'First day VISA promotion', 34.66, NOW(),'CREATED');
INSERT INTO tbl_rates (id, name, description, discount_rate, create_at, status) VALUES(3, 'Visa promotion2', 'Second day VISA promotion', 21.7, NOW(),'CREATED');
INSERT INTO tbl_rates (id, name, description, discount_rate, create_at, status) VALUES(4, 'Beginning of season2', 'Rate for beginning of season', 0, NOW(),'CREATED');

INSERT INTO tbl_prices (id, brand_id, start_date, end_date, price_list, product_id, priority, price,curr ) VALUES(1, 1 ,'2020-06-14 00:00:00', '2020-12-31 23:59:59', 1, 35455, 0, 35.50, 'EUR');
INSERT INTO tbl_prices (id, brand_id, start_date, end_date, price_list, product_id, priority, price,curr ) VALUES(2, 1 ,'2020-06-14 15:00:00', '2020-06-14 18:30:00', 2, 35455, 1, 25.45, 'EUR');
INSERT INTO tbl_prices (id, brand_id, start_date, end_date, price_list, product_id, priority, price,curr ) VALUES(3, 1 ,'2020-06-15 00:00:00', '2020-06-15 11:00:00', 3, 35455, 1, 30.50, 'EUR');
INSERT INTO tbl_prices (id, brand_id, start_date, end_date, price_list, product_id, priority, price,curr ) VALUES(4, 1 ,'2020-06-15 16:00:00', '2020-12-31 23:59:59', 4, 35455, 1, 38.95, 'EUR');
