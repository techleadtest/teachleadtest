package com.zara.dguimar.priceschedule;

import com.zara.dguimar.priceschedule.entity.Price;
import com.zara.dguimar.priceschedule.repository.PriceRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@DataJpaTest
public class PriceRepositoryMockTest {

    @Autowired
    private PriceRepository priceRepository;

    @Test
    public void whenFindByBrandId_thenReturnListPrice() {
        List<Price> priceList = priceRepository.findByBrandId(1L);
        Assertions.assertThat(priceList.size()).isEqualTo(4);
    }

    @Test
    public void whenFindAllPricesByRangeDate_thenReturnListPrice() throws ParseException {
        Date d = null;
        List<Price> priceList = null;

        // Para la fecha 2020-06-14 00:00:00 debe existir solo 1 precio
        d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-06-14 00:00:00");
        priceList = priceRepository.findAllPricesByRangeDate(d);
        Assertions.assertThat(priceList.size()).isEqualTo(1);

        // Para la fecha 2020-06-14 15:00:00 deben existir 2 precios
        d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-06-14 15:00:00");
        priceList = priceRepository.findAllPricesByRangeDate(d);
        Assertions.assertThat(priceList.size()).isEqualTo(2);

        // Para la fecha 2020-06-15 00:00:00 deben existir 2 precios
        d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-06-15 00:00:00");
        priceList = priceRepository.findAllPricesByRangeDate(d);
        Assertions.assertThat(priceList.size()).isEqualTo(2);

        // Para la fecha 2020-06-15 09:00:00 deben existir 3 precios
        d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-06-15 09:00:00");
        priceList = priceRepository.findAllPricesByRangeDate(d);
        Assertions.assertThat(priceList.size()).isEqualTo(2);
    }


    @Test
    public void whenFindPriorityPriceByRangeDateAndProductIdAndBrandId_thenReturnListPrice() throws ParseException {
        Date d = null;
        Price price = null;
        // Para la fecha 2020-06-14 15:00:00 debe retornar el precio con la prioridad 1
        d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-06-14 15:00:00");
        price = priceRepository.findPriorityPriceByRangeDateAndProductIdAndBrandId(d,35455L,1L);
        Assertions.assertThat(price.getPriority()).isEqualTo(1);

        // Para la fecha 2020-06-13 15:00:00 no existen precios posibles
        d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2020-06-13 15:00:00");
        price = priceRepository.findPriorityPriceByRangeDateAndProductIdAndBrandId(d,35455L,1L);
        Assertions.assertThat(price).isNull();
    }
}
