package com.zara.dguimar.brand.controller;

import com.zara.dguimar.brand.entity.Brand;
import com.zara.dguimar.brand.service.BrandService;
import com.zara.dguimar.brand.entity.Region;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/brands")
public class BrandRest {

    @Autowired
    BrandService brandService;

    // -------------------Retrieve All Brands--------------------------------------------

    @GetMapping
    public ResponseEntity<List<Brand>> listAllBrands(@RequestParam(name = "regionId" , required = false) Long regionId ) {
        List<Brand> brands =  new ArrayList<>();
        if (null ==  regionId) {
            brands = brandService.findBrandAll();
            if (brands.isEmpty()) {
                return ResponseEntity.noContent().build();
            }
        }else{
            Region Region= new Region();
            Region.setId(regionId);
            brands = brandService.findBrandsByRegion(Region);
            if ( null == brands) {
                log.error("Brands with Region id {} not found.", regionId);
                return  ResponseEntity.notFound().build();
            }
        }

        return  ResponseEntity.ok(brands);
    }

    // -------------------Retrieve Single Brand------------------------------------------

    @GetMapping(value = "/{id}")
    public ResponseEntity<Brand> getBrand(@PathVariable("id") long id) {
        log.info("Fetching Brand with id {}", id);
        Brand brand = brandService.getBrand(id);
        if (  null == brand) {
            log.error("Brand with id {} not found.", id);
            return  ResponseEntity.notFound().build();
        }
        return  ResponseEntity.ok(brand);
    }

    // -------------------Create a Brand-------------------------------------------

    @PostMapping
    public ResponseEntity<Brand> createBrand(@Valid @RequestBody Brand brand, BindingResult result) {
        log.info("Creating Brand : {}", brand);
        if (result.hasErrors()){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, this.formatMessage(result));
        }

       Brand brandDB = brandService.createBrand(brand);

        return  ResponseEntity.status( HttpStatus.CREATED).body(brandDB);
    }

    // ------------------- Update a Brand ------------------------------------------------

    @PutMapping(value = "/{id}")
    public ResponseEntity<?> updateBrand(@PathVariable("id") long id, @RequestBody Brand brand) {
        log.info("Updating Brand with id {}", id);

        Brand currentBrand = brandService.getBrand(id);

        if ( null == currentBrand) {
            log.error("Unable to update. Brand with id {} not found.", id);
            return  ResponseEntity.notFound().build();
        }
        brand.setId(id);
        currentBrand = brandService.updateBrand(brand);
        return  ResponseEntity.ok(currentBrand);
    }

    // ------------------- Delete a Brand-----------------------------------------

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Brand> deleteBrand(@PathVariable("id") long id) {
        log.info("Fetching & Deleting Brand with id {}", id);

        Brand brand = brandService.getBrand(id);
        if ( null == brand) {
            log.error("Unable to delete. Brand with id {} not found.", id);
            return  ResponseEntity.notFound().build();
        }
        brand = brandService.deleteBrand(brand);
        return  ResponseEntity.ok(brand);
    }

    private String formatMessage( BindingResult result){
        List<Map<String,String>> errors = result.getFieldErrors().stream()
                .map(err ->{
                    Map<String,String>  error =  new HashMap<>();
                    error.put(err.getField(), err.getDefaultMessage());
                    return error;

                }).collect(Collectors.toList());
        ErrorMessage errorMessage = ErrorMessage.builder()
                .code("01")
                .messages(errors).build();
        ObjectMapper mapper = new ObjectMapper();
        String jsonString="";
        try {
            jsonString = mapper.writeValueAsString(errorMessage);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jsonString;
    }

}
