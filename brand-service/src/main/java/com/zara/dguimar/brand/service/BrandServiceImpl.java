package com.zara.dguimar.brand.service;

import com.zara.dguimar.brand.entity.Brand;
import com.zara.dguimar.brand.repository.BrandRepository;
import com.zara.dguimar.brand.entity.Region;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Slf4j
@Service
public class BrandServiceImpl implements BrandService {

    @Autowired
    BrandRepository brandRepository;

    @Override
    public List<Brand> findBrandAll() {
        return brandRepository.findAll();
    }

    @Override
    public List<Brand> findBrandsByRegion(Region region) {
        return brandRepository.findByRegion(region);
    }

    @Override
    public Brand createBrand(Brand brand) {

        Brand brandDB = brandRepository.findByNumberID ( brand.getNumberID () );
        if (brandDB != null){
            return brandDB;
        }

        brand.setState("CREATED");
        brandDB = brandRepository.save (brand);
        return brandDB;
    }

    @Override
    public Brand updateBrand(Brand brand) {
        Brand brandDB = getBrand(brand.getId());
        if (brandDB == null){
            return  null;
        }
        brandDB.setName(brand.getName());
        brandDB.setDescription(brand.getDescription());
        brandDB.setEmail(brand.getEmail());
        brandDB.setLogo(brand.getLogo());

        return  brandRepository.save(brandDB);
    }

    @Override
    public Brand deleteBrand(Brand brand) {
        Brand brandDB = getBrand(brand.getId());
        if (brandDB ==null){
            return  null;
        }
        brand.setState("DELETED");
        return brandRepository.save(brand);
    }

    @Override
    public Brand getBrand(Long id) {
        return  brandRepository.findById(id).orElse(null);
    }
}
