package com.zara.dguimar.brand.service;

import com.zara.dguimar.brand.entity.Brand;
import com.zara.dguimar.brand.entity.Region;

import java.util.List;

public interface BrandService {

    public List<Brand> findBrandAll();
    public List<Brand> findBrandsByRegion(Region region);

    public Brand createBrand(Brand brand);
    public Brand updateBrand(Brand brand);
    public Brand deleteBrand(Brand brand);
    public Brand getBrand(Long id);



}
