package com.zara.dguimar.brand.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@Entity
@Table(name="tbl_brands")
public class Brand implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "El número de identificación fiscal no puede ser vacío")
    @Size( min = 8 , max = 50, message = "El tamaño del número de identificación fiscal debe tener entre 8 y 50 caractéres")
    @Column(name = "number_id" , unique = true , nullable = false)
    private String numberID;

    @NotEmpty(message = "El nombre de la cadena no puede ser vacío")
    @Column(name="name", nullable=false)
    private String name;

    private String description;

    @NotEmpty(message = "el correo no puede estar vacío")
    @Email(message = "no es un dirección de correo bien formada")
    @Column(unique=true, nullable=false)
    private String email;

    private String website;

    @Column(name="photo_url")
    private String logo;

    @NotNull(message = "la región no puede ser vacia")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "region_id")
    @JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
    private Region region;

    private String state;
}
