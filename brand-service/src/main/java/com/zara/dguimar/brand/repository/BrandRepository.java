package com.zara.dguimar.brand.repository;

import com.zara.dguimar.brand.entity.Brand;

import com.zara.dguimar.brand.entity.Region;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BrandRepository extends JpaRepository<Brand,Long> {
        public Brand findByNumberID(String numberID);
        public List<Brand> findByName(String name);
        public List<Brand> findByRegion(Region region);
}
