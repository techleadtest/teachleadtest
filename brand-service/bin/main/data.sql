INSERT INTO tbl_regions (id, name) VALUES (1, 'Sudamérica');
INSERT INTO tbl_regions (id, name) VALUES (2, 'Centroamérica');
INSERT INTO tbl_regions (id, name) VALUES (3, 'Norteamérica');
INSERT INTO tbl_regions (id, name) VALUES (4, 'Europa');
INSERT INTO tbl_regions (id, name) VALUES (5, 'Asia');
INSERT INTO tbl_regions (id, name) VALUES (6, 'Africa');
INSERT INTO tbl_regions (id, name) VALUES (7, 'Oceanía');
INSERT INTO tbl_regions (id, name) VALUES (8, 'Antártida');

INSERT INTO tbl_brands (id, number_id, name, description, email, website, photo_url, region_id, state) VALUES(1,'32404580', 'Zara', 'Casa de indumentaria', 'zara@gmail.com','http://www.zara.com','https://upload.wikimedia.org/wikipedia/commons/thumb/f/fd/Zara_Logo.svg/245px-Zara_Logo.svg.png',4,'CREATED');
