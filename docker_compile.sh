docker volume create --name gradle-cache
docker run --rm -v gradle-cache:/home/gradle/.gradle -v "$PWD":/app -w /app gradle:jdk11 gradle clean bootJar